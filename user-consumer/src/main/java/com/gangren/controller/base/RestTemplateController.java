package com.gangren.controller.base;

import com.gangren.api.base.BaseController;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author 钢人
 */
@RestController
@RequestMapping("/restTemplate")
public class RestTemplateController extends BaseController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/hello")
    @HystrixCommand(fallbackMethod = "errorCode")
    public String helloWorld(String content) {
        return restTemplate.getForObject("http://user-provider/provider/word?content=" + content, String.class);
    }

}
