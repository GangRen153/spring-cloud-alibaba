package com.gangren.controller.base;

import com.gangren.api.base.BaseController;
import com.gangren.api.base.provider.ProviderApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 钢人
 */
@RestController
@RequestMapping("/feignClient")
public class FeignClientController extends BaseController {

    @Autowired
    private ProviderApi providerApi;

    @GetMapping("/hello")
    public String hello(String content) {
        return providerApi.word(content);
    }

}
