package com.gangren.controller;

import com.gangren.api.base.BaseController;
import com.gangren.api.base.provider.ProviderApi;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 钢人
 */
@RefreshScope
@RestController
@RequestMapping("/consumer")
class UserConsumerController extends BaseController {

    @Value("${version}")
    private String version;

    @Autowired
    private ProviderApi providerApi;

    @GetMapping("/getVersion")
    public String getVersion() {
        return version;
    }

    @GetMapping("/feignClient")
    public String feignClient(String content) {
        return providerApi.word(content);
    }

    @GetMapping("/word")
    @HystrixCommand(fallbackMethod = "errorCode")
    public String word(@RequestParam(required = false) String content) {
        return "consumer回复: " + content + ", version: " + version;
    }

}