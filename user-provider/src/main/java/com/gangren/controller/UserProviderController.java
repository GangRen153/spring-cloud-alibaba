package com.gangren.controller;

import com.gangren.api.base.BaseController;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author 钢人
 */
@RestController
@RefreshScope
@RequestMapping("/provider")
public class UserProviderController extends BaseController {

    @Value("${version}")
    private String version;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/getVersion")
    public String getVersion() {
        return version;
    }

    @RequestMapping("/hello")
    @HystrixCommand(fallbackMethod = "errorCode")
    public String helloWorld(String content) {
        return restTemplate.getForObject("http://user-consumer/consumer/word?content=" + content, String.class);
    }

    /**
     * 测试
     * @param content content
     * @return str
     */
    @GetMapping("/feignProvider")
    public String feignProvider(@RequestParam(required = false) String content) {
        return "feignProvider 返回：" + content;
    }

    @GetMapping("/word")
    @HystrixCommand(fallbackMethod = "errorCode")
    public String word(@RequestParam(required = false) String content) {
        return "provider回复: " + content + ", version: " + version;
    }

}
