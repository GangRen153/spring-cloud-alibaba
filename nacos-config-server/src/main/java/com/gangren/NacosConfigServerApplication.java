package com.gangren;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 钢人
 */
@SpringBootApplication
public class NacosConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosConfigServerApplication.class, args);
    }

}
