package com.gangren.controller;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 钢人
 */
@RefreshScope
@Configurable
@RestController
public class NacosConfigController {

    @Value("${name}")
    private String name;

    @GetMapping("/getConfigInfo")
    public String getConfigInfo() {
        return name;
    }

}
