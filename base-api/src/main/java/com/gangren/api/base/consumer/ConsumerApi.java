package com.gangren.api.base.consumer;

import com.gangren.api.base.BaseFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 钢人
 */
@FeignClient(name = "user-consumer", fallbackFactory = BaseFallbackFactory.class)
public interface ConsumerApi {

    /**
     * 测试
     * @param content content
     * @return str
     */
    @GetMapping("/consumer/word")
    String word(@RequestParam(name = "content") String content);

}
