package com.gangren.api.base.provider;

import com.gangren.api.base.BaseFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 钢人
 */
@FeignClient(name = "user-provider", fallbackFactory = BaseFallbackFactory.class)
public interface ProviderApi {

    /**
     * 测试
     * @param content content
     * @return str
     */
    @GetMapping("/provider/word")
    String word(String content);

}
