package com.gangren.api.base;

import com.gangren.api.base.provider.ProviderApi;
import org.springframework.cloud.openfeign.FallbackFactory;

/**
 * 处理调用 FeignClient 接口失败的回调实现
 *
 * @author 钢人
 */
public class BaseFallbackFactory implements FallbackFactory<Object> {

    @Override
    public Object create(Throwable cause) {
        return null;
    }
}
